"# My project's README"  

### Informacoes basicas ###

* Version 1.0.0
* autor: Claudio Romano
* contato: claudioromano13@gmail.com
* whatsapp: 11 9 4880 3141

### O que é este projeto ? ###

* Este projeto faz parte do processo seletivo de uma empresa (confidencial)
* montar uma pagina a partir do arquivo em psd disponibilizado	

### O que foi feito ? ###

* Conceitos utilizados:
* Mobile First
* Html 5
* Css 3
* Sass
* Compass
* jQuery

