$(document).ready(function(){
	//loader
	$(".loader").delay(1000).fadeOut("slow");
	
	//btn back to top
    $('#back-top').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    // main menu effect
    $(".hamburguer").on("click",function(){
		$(this).toggleClass("open");
    	$("ul.menu").toggleClass("menu-open");
        $('.overlay').toggle();
    });

});